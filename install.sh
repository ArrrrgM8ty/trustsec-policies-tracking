#!/bin/bash

#must have root permissions
if [[ $(id -u) -ne 0 ]]; then
    echo "Must run script with root privileges."
    exit 1
fi

#install Rsyslog server
if [[ ! -z `systemctl status rsyslog.service | grep "not-found"` ]]; then
sudo add-apt-repository ppa:adiscon/v8-stable
sudo apt-get update
sudo apt-get install rsyslog
fi

#install Rsyslog Expansions
if [[ -z `apt list --installed | grep rsyslog-mongodb` ]]; then
sudo apt-get install rsyslog-mongodb
fi

if [[ -z `apt list --installed | grep rsyslog-mmjsonparse` ]]; then
sudo apt-get install rsyslog-mmjsonparse
fi

systemctl restart rsyslog.service

#install MongoDB
if [[ -z `which mongo` ]]; then
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 0C49F3730359A14518585931BC711F9BA15703C6
echo "deb [ arch=amd64,arm64 ] http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.4.list
sudo apt-get update
sudo apt-get install -y mongodb-org
fi

systemctl start mongod.service

#install Git
if [[ -z `which git` ]]; then
sudo apt install git
fi

#verify the Python version
if [[ ! `python3 --version | cut -d '.' -f 2` -ge 5 ]]; then
    sudo apt-get -y install python3.5
fi

#install pip3
if [[ -z `pip3 --version` ]]; then
    sudo apt-get -y install python3-pip
fi

#upgrade pip3 to latest version
if [[ `pip3 --version | cut -d' ' -f2 | cut -d'.' -f1` -lt 9 ]]; then
    sudo pip3 install --upgrade pip
fi

#install Python Virtual ENVironment (if not already installed)
if [[ -z `pip3 freeze | grep virtualenv` ]]; then
    pip3 install virtualenv
fi

# install rfc5424-logging-handler for emulator
sudo pip3 install rfc5424-logging-handler


#start getting stuff ready
cd ~
git clone https://ArrrrgM8ty@bitbucket.org/ArrrrgM8ty/trustsec-policies-tracking.git
cd ise-project
git checkout development
virtualenv venv
venv/bin/pip3 install -r req.txt

sudo chown -R `whoami`:`whoami` venv/

#TODO: add nginx and uwsgi installation and condifguration

# Install Rsyslog config file
# backup original Rsyslog config file. Assume we are in ise-project dir
mkdir -p rsyslog-backup
cd rsyslog-backup
sudo service rsyslog stop
sudo cp /etc/rsyslog.conf ./rsyslog.conf.backup
cd ..
sudo cp rsyslog-conf/rsyslog.conf /etc/rsyslog.conf
sudo service rsyslog start

#start mongo in case it's not running
sudo service mongod start

echo
echo "====================="
echo "INSTALLATION COMPLETE"
echo "====================="
echo
