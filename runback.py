#!venv/bin/python3

#
# This is the main entry point of the backend.
#
import time
import signal
import logging
from datetime import datetime
from src.db_updater import DbUpdater


def handler(signum, frame):
    global abort_flag
    abort_flag = 1
    return


def set_logger():
    my_logger = logging.getLogger('BACKEND')
    my_logger.setLevel(logging.DEBUG)
    fh = logging.FileHandler('logs/' + datetime.now().strftime('policyProjectlogfile_%H_%M_%d_%m_%Y.log'))
    fh.setLevel(logging.DEBUG)
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)
    # add the handlers to the logger
    my_logger.addHandler(fh)
    my_logger.addHandler(ch)
    my_logger.propagate = False
    return my_logger


if __name__ == '__main__':

    abort_flag = 0
    dbu = DbUpdater()
    signal.signal(signal.SIGINT, handler)
    logger1 = set_logger()
    logger1.info("Backend is starting...")
    while not abort_flag:
        try:
            dbu.db_parse_walk()
            time.sleep(0.5)
        except Exception as e:
            logger1.error('Unknown error!')
            dbu.close()
            exit()
    logger1.info('Exiting gracefully. Bye.')
    dbu.close()
