import os
import random
import string
import logging
from time import sleep
from rfc5424logging import Rfc5424SysLogHandler, Rfc5424SysLogAdapter

template_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'syslog-templates')


class SyslogEmulator:
    def __init__(self, templates_path=template_dir, address="127.0.0.1"):
        # Read the template files
        for root, dirs, filenames in os.walk(templates_path):
            for f in filenames:
                try:
                    template = open(os.path.join(root, f), 'r')
                except FileNotFoundError as e:
                    print('File cannot be opened:', e)
                    exit()
                    # only one line is expected
                line = template.readline().rstrip()
                if "1.template.log" in template.name:
                    self._authentication_msg = line
                elif "2.template.log" in template.name:
                    self._sgt_request_msg = line
                elif "3.template.log" in template.name:
                    self._sgacl_request_msg = line
                elif "4.template.log" in template.name:
                    self._accounting_start_msg = line
                elif "5.template.log" in template.name:
                    self._accounting_stop_msg = line
                elif "6.template.log" in template.name:
                    self._sgt_request_column_msg = line
                else:
                    print('Unknown file names')
                    template.close()
                    exit()
                template.close()

        # Config the logger
        self._logger = logging.getLogger('syslogemu')
        self._logger.setLevel(logging.INFO)

        sh = Rfc5424SysLogHandler(address=(address, 514), enterprise_id="32473")
        self._logger.addHandler(sh)
        self._adapter = Rfc5424SysLogAdapter(self._logger, enable_extra_levels=True)

    def _get_unique_username(self):
        first_names = {"jay", "jim", "roy", "axel", "billy", "charlie", "jax", "gina", "paul", "ringo", "ally", "nicky",
                       "cam", "ari", "trudie", "cal", "carl", "lady", "lauren", "ichabod", "arthur", "ashley", "drake",
                       "kim", "julio", "lorraine", "floyd", "janet", "lydia", "charles", "pedro", "bradley", "jojo",
                       "moshe"}
        last_names = {"koko", "barker", "style", "spirits", "murphy", "blacker", "bleacher", "rogers", "warren",
                      "keller", "gogo", "hoho", "fofo", "yoyo", "shishkabab"}
        tries = 50
        while tries:
            num = random.randrange(10, 100, 1)
            first = random.sample(first_names, 1)[0]
            last = random.sample(last_names - {first}, 1)[0]
            username = first + last + str(num)
            if username not in self._used_usernames:
                self._used_usernames.append(username)
                return username
        tries = tries - 1
        return None

    def _get_unique_ip(self):
        tries = 50
        while tries:
            octet1 = random.randrange(1, 256, 1)
            octet2 = random.randrange(1, 256, 1)
            octet3 = random.randrange(1, 256, 1)
            octet4 = random.randrange(1, 256, 1)

            ip = str(octet1) + '.' + str(octet2) + '.' + str(octet3) + '.' + str(octet4)
            if ip not in self._used_ips:
                self._used_ips.append(ip)
                return ip
        tries = tries - 1
        return None

    def _get_unique_sgt(self):
        tries = 50
        while tries:
            sgt = str(random.randrange(1, 10000, 1)).rjust(4, '0')
            # versioned = sgt + "-" + str(random.randrange(1, 100, 1))

            if sgt not in self._used_sgts:
                self._used_ips.append(sgt)
                # return sgt, versioned
                return sgt
        tries = tries - 1
        return None

    def _get_unique_session_id(self):
        tries = 5
        while tries:
            session_id = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(51))
            if session_id not in self._used_session_ids:
                self._used_ips.append(session_id)
                return session_id
        tries = tries - 1
        return None

    def _get_unique_mac(self):
        tries = 5
        while tries:
            mac = '02-02-02-%02X-%02X-%02X' % (random.randrange(0, 255, 1),
                                               random.randrange(0, 255, 1), random.randrange(0, 255, 1))
            if mac not in self._used_macs_ids:
                self._used_ips.append(mac)
                return mac
            tries = tries - 1
        return None

    def _get_sgacl_content(self):

        key = "cts:rbacl-ace#"
        rules = {"deny udp src eq * dst eq @",
                 "deny tcp src eq * dst eq @",
                 "allow tcp src eq * dst eq @",
                 "allow udp src eq * dst eq @",
                 "deny igmp",
                 "deny icmp",
                 "permit ip"}

        sgacl_content = dict()
        for i in range(1, 5, 1):
            first = random.sample(rules, 1)[0]
            if "deny" or "allow" in first:
                src = random.randrange(80, 2048, 1)
                dest = random.randrange(80, 2048, 1)
                first = first.replace("*", str(src))
                first = first.replace("@", str(dest))
            sgacl_content[key + str(i)] = first
        return sgacl_content

    def _get_unique_sgacl(self):
        tries = 5
        while tries:
            sgacl = ''.join(random.choice(string.ascii_uppercase) for _ in range(3))
            sgacl = sgacl + "_SGACL"

            if sgacl not in self._used_sgacls:
                self._used_sgacls.append(sgacl)
                return sgacl
            tries = tries - 1
        return None

    def send_user_accounting_start(self, username, user_ip, user_mac, session_id, access_ip):
        self._send_user_accounting(username, user_ip, user_mac, session_id, access_ip, 3000)

    def send_user_accounting_stop(self, username, user_ip, user_mac, session_id, access_ip):
        self._send_user_accounting(username, user_ip, user_mac, session_id, access_ip, 3001)

    def _send_user_accounting(self, username, user_ip, user_mac, session_id, access_ip, msgid):
        # Fields to change in the template:
        # User-Name=$_USER_$
        # Session ID: $_SESSION_ID_$
        # Calling - Station - ID =$_USER_MAC_$
        # NAS - IP - Address =$_ACCESS_IP_$

        replace = {"$_USER_$": username, "$_USER_IP_$": user_ip, "$_SESSION_ID_$": session_id, "$_USER_MAC_$": user_mac,
                   "$_ACCESS_IP_$": access_ip}

        # Sends the syslog
        if msgid == 3000:
            payload = self._replace_all(self._accounting_start_msg, replace)
            self._adapter.notice(payload, msgid=3000)
        elif msgid == 3001:
            payload = self._replace_all(self._accounting_stop_msg, replace)
            self._adapter.notice(payload, msgid=3001)
        else:
            return

    def send_start_authentication(self, username, user_ip, user_mac, session_id, access_ip, sgt):
        # Fields to change in the template:
        # User-Name=$_USER_$
        # Session ID: $_SESSION_ID_$
        # Calling - Station - ID =$_USER_MAC_$
        # NAS - IP - Address =$_ACCESS_IP_$
        # cts: security - group - tag =$_USER_SGT_VER_$

        replace = {"$_USER_$": username, "$_USER_IP_$": user_ip, "$_SESSION_ID_$": session_id, "$_USER_MAC_$": user_mac,
                   "$_ACCESS_IP_$": access_ip, "$_USER_SGT_VER_$": sgt + "-00"}

        # Sends the syslog
        payload = self._replace_all(self._authentication_msg, replace)
        self._adapter.notice(payload, msgid=5200)

    def send_sgt_request(self, enforcer_ip, user_sgt, dest_sgt, sgacl):
        # NAS-IP-Address=$_ENFORCING_IP_$
        # cisco-av-pair=cts-rbacl-source-list=$_DEST_SGT_$
        # cts: src - dst - rbacl =$_USER_SGT_VER_$-00 -$_DEST_SGT_VER_$-00 -$_DEST_SGACL_VER_$

        replace = {"$_ENFORCING_IP_$": enforcer_ip, "$_USER_SGT_VER_$": user_sgt + "-00", "$_USER_SGT_$": user_sgt,
                   "$_DEST_SGT_VER_$": dest_sgt + "-00", "$_DEST_SGACL_VER_$": sgacl + "-00", "$_DEST_SGT_$":
                       dest_sgt}

        payload = self._replace_all(self._sgt_request_msg, replace)
        self._adapter.notice(payload, msgid=5233)
        return

    def send_sgt_request_column(self, enforcer_ip, dest_sgt, user_sgt1, sgacl1, user_sgt2, sgacl2):
        # NAS-IP-Address=$_ENFORCING_IP_$
        # cisco-av-pair=cts-rbacl-source-list=$_DEST_SGT_VER_$
        # Response:
        # 1. cisco-av-pair=cts:src-dst-rbacl = $_USER_SGT_VER_$-00-$_DEST_SGT_$-02-00-v4v6-$_DEST_SGACL_VER_$
        # 2. cisco-av-pair=cts:src-dst-rbacl = $_USER_SGT_VER2_$-00-$_DEST_SGT_$-02-00-v4v6-$_DEST_SGACL_VER2_$

        replace = {"$_ENFORCING_IP_$": enforcer_ip, "$_USER_SGT_VER_$": user_sgt1 + "-00", "$_USER_SGT_$": user_sgt1,
                   "$_DEST_SGT_VER_$": dest_sgt + "-00", "$_DEST_SGT_$": dest_sgt, "$_DEST_SGACL_VER_$": sgacl1 + "-00",
                   "$_USER_SGT_VER2_$": user_sgt2 + "-00", "$_USER_SGT2_$": user_sgt2, "$_DEST_SGACL_VER2_$": sgacl2
                                                                                                              + "-00"
                   }

        payload = self._replace_all(self._sgt_request_column_msg, replace)
        self._adapter.notice(payload, msgid=5233)
        return

    def send_sgacl_request(self, enforcing_ip, dest_sgacl, sgacl_content):
        # NAS - IP - Address =$_ENFORCING_IP_$
        # cisco - av - pair = cts - rbacl =$_DEST_SGACL_$
        # cisco - av - pair = cts:rbacl =$_DEST_SGACL_VER_$; $_SGACL_CONTENT_STRING_$

        contenent_str = ""
        for key, val in sgacl_content.items():
            contenent_str = contenent_str + "cisco-av-pair=" + key + "=" + str(val) + "; "

        replace = {"$_ENFORCING_IP_$": enforcing_ip, "$_DEST_SGACL_$": dest_sgacl,
                   "$_DEST_SGACL_VER_$": dest_sgacl + "-00", "$_SGACL_CONTENT_STRING_$": contenent_str}

        payload = self._replace_all(self._sgacl_request_msg, replace)
        self._adapter.notice(payload, msgid=5233)
        return

    def _replace_all(self, text, dic):
        for i, j in dic.items():
            text = text.replace(i, j)
        return text

    def setup(self, nusers, nresources, naccess, nenforcer):
        """ Setup a scenario environment.
        :users: number of users in the ise system
        :access: number of access devices in the system
        :enforcer: number of enforcing devices in the system
        """
        assert nusers > 0 and nresources > 0 and naccess > 0 and nenforcer > 0, "All inputs must be positive."

        users = []
        resources = []
        access = []
        enforcer = []
        mod = random.randint(2, 5)
        reminder = random.randint(0, mod - 1)
        counter = 0

        print('Setting up simulation environment.')
        input('\nClick to add users to the system...')
        for _ in range(nusers):
            user = {
                'username': self._get_unique_username(),
                'ip': self._get_unique_ip(),
                'mac': self._get_unique_mac(),
                'session': self._get_unique_session_id(),
                'sgt': self._get_unique_sgt(),
                'insession': False
            }
            users.append(user)
        print('Added %d users to the system.' % nusers)
        for user in users:
            print('\t%s    %s' % (user['username'], user['sgt']))

        input('\nClick to add resources to the system...')
        for _ in range(nresources):
            resource = {
                'sgt': self._get_unique_sgt(),
                'sgacl': self._get_unique_sgacl()
            }
            resources.append(resource)
        print('Added %d resources to the system.' % nresources)
        for res in resources:
            print('\t%s' % res['sgt'])

        input('\nClick to add Access Devices to the system...')
        for _ in range(naccess):
            access.append(self._get_unique_ip())
        print('Added %d Access Devices to the system.' % naccess)
        for acc in access:
            print('\t%s' % acc)

        input('\nClick to add Enforcing Devices to the system...')
        for i in range(nenforcer):
            if i % mod == reminder:
                tmp = random.sample(access, 1)[0]
                if tmp not in enforcer:
                    enforcer.append(tmp)
                    counter += 1
            else:
                enforcer.append(self._get_unique_ip())
        print('Added %d Enforcing Devices to the system.' % nenforcer)
        if counter:
            print('%d of them are also Access Devices.' % counter)
        for enf in enforcer:
            print('\t%s' % enf)

        print('Setup complete.')
        return users, resources, access, enforcer

    def run_scenario_1(self):
        """
        Runs a default predefined scenario
        :return: 
        """
        print('')
        print('===================')
        print('STARTING SCENARIO 1')
        print('===================')
        print('')

        users, resources, access, enforcer = self.setup(random.randint(2, 5), random.randint(2, 4), \
                                                        random.randint(2, 4), random.randint(2, 4))

        for i in range(len(users)):
            user = random.sample(users, 1)[0]
            while user['insession']:
                user = random.sample(users, 1)[0]
            resource = random.sample(resources, 1)[0]
            acc_dev = random.sample(access, 1)[0]
            enf_dev = random.sample(enforcer, 1)[0]

            # send accounting start for a user
            input('\nSend "Accounting Start" for %s through access device %s' % (user['username'], acc_dev))
            self.send_user_accounting_start(user['username'], user['ip'], user['mac'], user['session'], acc_dev)
            user['insession'] = True

            # Send an authentication of the user
            input('\nSend "Authentication" for %s' % user['username'])
            self.send_start_authentication(user['username'], user['ip'], user['mac'], user['session'], acc_dev,
                                           user['sgt'])

            # send a sgt request
            input('\nSend SGT request.\nFrom %s\nParameters:\n\tuser-sgt %s\n\tresource-sgt %s' % (
                enf_dev, user['sgt'], resource['sgt']))
            self.send_sgt_request(enf_dev, user['sgt'], resource['sgt'], resource['sgacl'])

            # send content request for recourse1
            sgacl_content = self._get_sgacl_content()
            input('\nSend SGACL Content request.\nFrom: %s\nParameters:\n\tuser-sgt %s\n\tresource-sgt %s\n\tsgacl %s' % \
                  (enf_dev, user['sgt'], resource['sgt'], resource['sgacl']))
            print("\nEnforcing device requested matrix entry: [%s]X[%s]->%s" % (
                user['sgt'], resource['sgt'], resource['sgacl']))
            self.send_sgacl_request(enf_dev, resource['sgacl'], sgacl_content)

            if i % 2 == 0:
                input('\nSend "Accounting Stop" for %s through access device %s' % (user['username'], acc_dev))
                self.send_user_accounting_stop(user['username'], user['ip'], user['mac'], user['session'], acc_dev)
                user['insession'] = False
        return

    def scenario_early_accounting_stop(self):
        """
        this scenario will emulate an accounting stop for a non-existing session and test to see that the keeper
        restores this log later on.
        :return:
        """
        # Create user
        user = self._get_unique_username()
        user_ip = self._get_unique_ip()
        user_mac = self._get_unique_mac()
        user_session = self._get_unique_session_id()
        user_sgt = self._get_unique_sgt()

        # Access device
        access_device_ip = self._get_unique_ip()

        print("User is:", user, "IP is:", user_ip, "SGT is:", user_sgt)
        input("Press any key to send accounting stop for non-existing session...")
        self.send_user_accounting_stop(user, user_ip, user_mac, user_session, access_device_ip)
        input("Press any key to send accounting start and auth for user.")
        sleep(1)
        # send accounting start
        self.send_user_accounting_start(user, user_ip, user_mac, user_session, access_device_ip)
        # Send an authentication of user
        self.send_start_authentication(user, user_ip, user_mac, user_session, access_device_ip, user_sgt)

        print("Verify that user:", user_ip, "no longer has a session.")
        return

    def scenario_sgt_request_with_column(self):
        """
        this scenario will emulate a SGT request with a column returned in response
        :return:
        """

        # enforcing device
        enforcing_device_ip = self._get_unique_ip()
        # resource SGT
        resource_sgt = self._get_unique_sgt()
        # destination SGACL for

        user_sgt_1 = self._get_unique_sgt()
        user_sgt_2 = self._get_unique_sgt()
        sgacl1 = self._get_unique_sgacl()
        sgacl2 = self._get_unique_sgacl()

        input("Press any key to send SGT request.")
        print("enforcer is:", enforcing_device_ip, "Destination SGT is:", resource_sgt)
        print("user1 SGT:", user_sgt_1, "user2 SGT:", user_sgt_2, "SGACL1:", sgacl1, "SGACL2:", sgacl2)
        self.send_sgt_request_column(enforcing_device_ip, resource_sgt, user_sgt_1, sgacl1, user_sgt_2, sgacl2)

        print("Verify that enforcing device:", enforcing_device_ip, " has two entries in matrix.")
        return

    def deterministic_scenario_access_denied(self):

        user = 'bob'
        user_ip = '192.168.0.10'
        user_mac = '02-02-02-02-02-02'
        user_session = self._get_unique_session_id()
        user_sgt = '0010'

        access_device_ip = '192.168.0.1'

        input("Press any key to send accounting start...\n")
        # send accounting start
        self.send_user_accounting_start(user, user_ip, user_mac, user_session, access_device_ip)
        input("Press any key to send authentication...\n")
        # Send an authentication of user
        self.send_start_authentication(user, user_ip, user_mac, user_session, access_device_ip, user_sgt)

        # Enforcing device

        resource_sgt = '0200'
        # resource_sgacl = 'XX_SGACL'
        enforcing_ip = '192.168.0.100'

        wrong_user_sgt_1 = '0003'
        wrong_user_sgt_2 = '0004'
        wrong_user_sgacl_1 = 'QQ_SGACL'
        wrong_user_sgacl_2 = 'PP_SGACL'
        content_1 = self._get_sgacl_content()
        content_2 = self._get_sgacl_content()

        input("Press any key to send sgt request...\n")
        # send a sgt column request for resource, but with other SGTs (simulate user has no access!)
        self.send_sgt_request_column(enforcing_ip, resource_sgt, wrong_user_sgt_1, wrong_user_sgacl_1,
                                     wrong_user_sgt_2, wrong_user_sgacl_2)

        self.send_sgacl_request(enforcing_ip, wrong_user_sgacl_1, content_1)
        self.send_sgacl_request(enforcing_ip, wrong_user_sgacl_2, content_2)

        input("Press any key to send accounting stop...\n")
        self.send_user_accounting_stop(user, user_ip, user_mac, user_session, access_device_ip)

        return

    def deterministic_scenario_normal(self):

        user = 'User'
        user_ip = '192.168.11.39'
        user_mac = '02-02-02-02-02-02'
        user_session = self._get_unique_session_id()
        user_sgt = '0030'

        access_device_ip = '192.168.11.150'

        input("Press any key to send accounting start...\n")
        # send accounting start
        self.send_user_accounting_start(user, user_ip, user_mac, user_session, access_device_ip)
        input("Press any key to send authentication...\n")
        # Send an authentication of user
        self.send_start_authentication(user, user_ip, user_mac, user_session, access_device_ip, user_sgt)

        # Enforcing device

        resource_sgt = '0111'
        resource_sgacl = 'QT_SGACL'
        enforcing_ip = '192.168.11.110'

        input("Press any key to send sgt request...\n")
        # send a sgt column request for resource, but with other SGTs (simulate user has no access!)
        self.send_sgt_request(enforcing_ip, user_sgt, resource_sgt, resource_sgacl)
        input("Press any key to send SGACL content request...\n")
        content = self._get_sgacl_content()
        self.send_sgacl_request(enforcing_ip, resource_sgacl, content)

        input("Press any key to send accounting stop...\n")
        self.send_user_accounting_stop(user, user_ip, user_mac, user_session, access_device_ip)
        print("end of demo")
        return

    _used_macs_ids = []
    _used_session_ids = []
    _used_sgts = []
    _used_ips = []
    _used_usernames = []
    _used_sgacls = []

    _authentication_msg = ""
    _sgt_request_msg = ""
    _sgacl_request_msg = ""
    _accounting_start_msg = ""
    _accounting_stop_msg = ""
    _sgt_request_column_msg = ""

    _logger = None
    _adapter = None


if __name__ == "__main__":
    emu = SyslogEmulator()
    print('')
    print('This ISE emulator is playing the role of the ISE machine in the TrustSec network.')
    print('All of the logs that are sent to the syslog server match the format that was provided in the Word Document.')
    print('The actual data, e.g. IP addresses, usernames etc., are randomized.')
    input('Press any key to start simulating "scenario 1"...')
    emu.run_scenario_1()
    # emu.scenario_early_accounting_stop()
    # emu.scenario_sgt_request_with_column()


    # stuff for Technion demo-day
    # emu.deterministic_scenario_normal()
    # emu.deterministic_scenario_access_denied()
