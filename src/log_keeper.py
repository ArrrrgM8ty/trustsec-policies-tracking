from src.ise_strings import RawCollections
from time import monotonic
import logging

logger3 = logging.getLogger('BACKEND.keeper')


class LogKeeper:
    """ This class will handle logs that arrived out of sync. These logs will be kept for a while, and then attempt
    to re-insert them to the database consistently to the other data, hopefully in a successful manner this time.
    Logs that keep coming back to the keeper will be dropped eventually.

    The keeper holds two lists: one for accounting stop logs and one for SGACL content requests, since these logs
    depend on previous logs that need to arrive. Each list item is a tuple of (dict_log, time_ticks).
    delta - seconds to hold the log in the keeper before trying to reinsert.
    visit_threshold - the log may leave the keeper after delta seconds, but may return. After visit_threshold we drop
    that log.
    """

    def __init__(self, delta=2, visit_threshold = 2):
        """
        :param delta: Minimal time in seconds before the kept logs will attempt to re-parse.
        """
        self._time_sync_delta = delta
        self._visit_threshold = visit_threshold
        self._enum_to_list_converter = {RawCollections.ACCOUNTING_STOP: self._acc_stop_wait_list,
                                        RawCollections.SGACL_CONTENT: self._sgacl_req_wait_list}

    def keep(self, log, timestamp, enum_type):
        """
        keep a log.
        :param log: parsed log dictionary to keep. The original timestamp will be added to the log dictionary.
        :param timestamp: log's original send time.
        :param enum_type: the enum type of the collections the log belongs to.
        :return: None
        """
        for (x, y) in self._enum_to_list_converter[enum_type]:
            if x['raw_id'] == log['raw_id']:
                return
        # have this log visited the keeper in the past?
        visits = log.get("keeper_visit_count", 0)
        if visits >= self._visit_threshold:
            logger3.info("log %s exceeded visit count to the keeper. Dropping.", log['raw_id'])
            return
        else:
            visits = visits + 1
            log["keeper_visit_count"] = visits

        log["time_stamp"] = timestamp
        self._enum_to_list_converter[enum_type].append((log, monotonic()))
        logger3.info("%s log was added to keeper", str(enum_type))

    def get_need_to_sync(self):
        """
        returns a list of logs that need to try to reinsert consistently
        :return: a list of logs. Each list item is (log, collection enum)
        """
        res = list()
        for enum, lst in self._enum_to_list_converter.items():
            for (log, ticks) in lst:
                if (monotonic() - ticks) >= self._time_sync_delta:
                    res.append((log, enum))
                    lst.remove((log, ticks))
        num = len(res)
        if num > 0:
            logger3.info("Keeper found %s logs to restore", str(num))
        return res

    _acc_stop_wait_list = list()
    _sgacl_req_wait_list = list()
    _time_sync_delta = 0
    _visit_threshold = 0
    _enum_to_list_converter = dict()
