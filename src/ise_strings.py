from enum import Enum


class DatabaseStrings:
    DB_NAME = 'ise_db'
    DB_RAW_NAME = 'raw_syslogs'
    RAW_COLLECTION_ACCOUNTING_START_NAME = 'raw_accounting_start'
    RAW_COLLECTION_ACCOUNTING_STOP_NAME = 'raw_accounting_stop'
    RAW_COLLECTION_AUTHENTICATION_NAME = 'raw_authentication'
    RAW_COLLECTION_REQUEST_POLICY_NAME = 'raw_sgt_reqest'
    RAW_COLLECTION_SGACL_NAME = 'raw_sgacl_reqest'

    ISE_COLL_SESSIONS = 'active_sessions'
    ISE_COLL_ACCESS = 'access_devices'
    ISE_COLL_ENFORCING = 'enforcing_devices'


class RawCollections(Enum):
    ACCOUNTING_START = 1
    ACCOUNTING_STOP = 2
    AUTHENTICATION = 3
    REQUEST_POLICY = 4
    SGACL_CONTENT = 5
