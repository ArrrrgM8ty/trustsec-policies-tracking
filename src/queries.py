#!/usr/bin/env python

from src.db_updater import DatabaseStrings
from pymongo import errors as mongoerrors


class Queries:
    """
    This class hold all necessary queries for the database.
    """

    @staticmethod
    def find_access_device_by_ip(db, ip):
        """
        Finds an access device in the database.
        :param db: database handler
        :param ip: requested ip address
        :return: 
        """
        try:
            return db[DatabaseStrings.ISE_COLL_ACCESS].find_one(
                {"NAS-IP-Address": {"$eq": ip}})
        except mongoerrors.PyMongoError as e:
            return None

    @staticmethod
    def find_enforcing_device_by_ip(db, ip):
        """
        Finds an enforcing device in the database.
        :param db: database handler
        :param ip: requested ip address
        :return: 
        """
        try:
            return db[DatabaseStrings.ISE_COLL_ENFORCING].find_one(
                {"NAS-IP-Address": {"$eq": ip}})
        except mongoerrors.PyMongoError as e:
            return None

    @staticmethod
    def find_devices_by_sgt(db, sgt, device_type):
        """
        
        :param db: 
        :param sgt: 
        :param device_type: 
        :return: 
        """
        try:
            if device_type == "Access":
                return db[DatabaseStrings.ISE_COLL_ACCESS].find({"SGTs.sgt_id": {"$eq": str(sgt)}})
            elif device_type == "Enforcer":
                return list(db[DatabaseStrings.ISE_COLL_ENFORCING].find(
                    {"$or": [{"Matrix.source_sgt": str(sgt)}, {"Matrix.destination_sgt": str(sgt)}]}))
            else:
                return None
        except mongoerrors.PyMongoError as e:
            return None
