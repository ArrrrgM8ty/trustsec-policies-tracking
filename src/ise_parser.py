import json


class SyslogParser():
    """Parser class for ISE syslogs.
    Feed the Constructor with raw syslog text and call
    the parse method to get the results.
    """

    def __init__(self, log):
        content = log.split(', ')
        # header = self.__parse_header(content[0])
        body = self.__parse_body(content[1:])
        # header.update(body)
        # self.result = header
        self.result = body

    def parse(self):
        """Call this method to get the results of the
        parsing action. the returned value is
        a dictionary.
        """
        return self.result

    def __str__(self):
        """Pretty print the parsing results.
        """
        return json.dumps(self.result, indent=4)

    def __parse_header(self, header):
        """Parse the first part of the syslog - the header.
        This part contains the time stamp of the syslog, the serial number
        of the log and the code and type of the message.
        """
        temp = header.split(' ')
        date, time, zone, serial, code, hdr_type, msg = temp[:7]
        msg_content = ' '.join(temp[7:])
        return {
            'time stamp': ' '.join([date, time, zone]),
            'serial': int(serial),
            'code': int(code),
            'type': hdr_type,
            msg.replace(':', ''): msg_content
        }

    def __parse_body(self, body):
        """Parse the second part of the syslog - the body.
        Handle each line separately and insert the individual
        result to the output dictionary.
        """
        body_json = {}
        for line in body:
            key, val = self.__parse_single_line(line)
            self.__insert_to_dict(body_json, key, val)
        return body_json

    def __parse_single_line(self, line):
        """Parse a single line of the syslog.
        A line is defined as the value of the syslog file.
        """
        if line.count('=') == 1:
            return line.split('=')

        elif line.count('=') == 2:
            master_key, key, val = line.split('=')
            return master_key, {key: val}

        elif 'Response' in line:
            return self.__handle_response(line)

        else:
            temp = line.split('=')
            key = temp[0]
            val = '='.join(temp[1:])
            return key, val

    def __handle_response(self, response):
        """Special handle of the 'Response' part of the body."""
        response_dict = {}
        master_key, rest = response.split('={')
        rest = rest.replace('},', '').strip()
        for entry in rest.split('; '):
            key, val = self.__parse_single_line(entry)
            self.__insert_to_dict(response_dict, key, val)
        return master_key, response_dict

    def __insert_to_dict(self, dictionary, key, val):
        """Insertion of a key:value pair to the given
        dictionary with compliance to the current status
        of the dictionary.
        """
        if key in dictionary.keys():
            try:
                if isinstance(val, dict):
                    # we can't just merge two dictionaries since keys may overwrite each other
                    dictionary[key] = self.__merge_dicts_no_overwrite(dictionary[key], val)
                else:
                    dictionary[key].append(val)
            except AttributeError:
                dictionary[key] = [dictionary[key], val]
        else:
            dictionary[key] = val

    def __merge_dicts_no_overwrite(self, big_dic, small_dic):
        """
        Merges dictionaries to a single dictionary, concatenates values with the same key.
        :param big_dic: first dictionary to be merged.
        :param small_dic: second dictionary to be merged.
        :return: Merged dictionary.
        """
        result = big_dic.copy()
        for key, val in (small_dic.items()):
            if key in result:
                if isinstance(result[key], list):
                    result[key].append(val)
                else:
                    result[key] = [result[key], val]
            else:
                result[key] = val

        return result
