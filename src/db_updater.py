from pymongo import MongoClient
from pymongo import ASCENDING
from pymongo import errors as mongoerrors
from src.ise_parser import SyslogParser
from src.ise_strings import *
from src.log_keeper import LogKeeper
import logging

logger2 = logging.getLogger('BACKEND.db_updater')
DEBUG_DONT_DELETE_RAW = False


class DbUpdater:
    """
    This is the backend class. This handles parsing and updates to incoming logs.
    """
    _N_PULL_RECORDS_LIMIT = 1000

    def __init__(self, host='localhost', user=None, password=None):
        try:
            self._client = MongoClient(host, 27017)
            self._client.admin.command('ismaster')

        except mongoerrors.ConnectionFailure as e:
            logger2.error("Unable to connect to host. Will try to reconnect...: %s", e)
        except mongoerrors.ServerSelectionTimeoutError as e:
            logger2.error("Unable to connect. Timed out: %s", e)
        # logger2.info("Connected to database server.")
        try:
            self._raw_db = self._client[DatabaseStrings.DB_RAW_NAME]
            self._ise_db = self._client[DatabaseStrings.DB_NAME]

            if user and password:
                self._client[DatabaseStrings.DB_RAW_NAME].authenticate(user, password, mechanism='SCRAM-SHA-1')
                self._client[DatabaseStrings.DB_NAME].authenticate(user, password, mechanism='SCRAM-SHA-1')
        except mongoerrors.PyMongoError as e:
            self.close(self)
            self._raw_db = None
            self._ise_db = None
            logger2.error("Unable to get database: %s", e)

        # init the late/early log keeper
        self._keeper = LogKeeper()

        # dictionaries for fast conversion
        self._raw_collection_names_list = [DatabaseStrings.RAW_COLLECTION_ACCOUNTING_START_NAME,
                                           DatabaseStrings.RAW_COLLECTION_ACCOUNTING_STOP_NAME,
                                           DatabaseStrings.RAW_COLLECTION_AUTHENTICATION_NAME,
                                           DatabaseStrings.RAW_COLLECTION_REQUEST_POLICY_NAME,
                                           DatabaseStrings.RAW_COLLECTION_SGACL_NAME]

        self._parse_caller = {DatabaseStrings.RAW_COLLECTION_ACCOUNTING_START_NAME: self._handle_accounting_start_event,
                              DatabaseStrings.RAW_COLLECTION_ACCOUNTING_STOP_NAME: self._handle_accounting_stop_event,
                              DatabaseStrings.RAW_COLLECTION_AUTHENTICATION_NAME: self._handle_authentication_event,
                              DatabaseStrings.RAW_COLLECTION_REQUEST_POLICY_NAME: self._handle_sgt_request_event,
                              DatabaseStrings.RAW_COLLECTION_SGACL_NAME: self._handle_sgacl_request_event}

        self._raw_coll_enum_convert = {
            DatabaseStrings.RAW_COLLECTION_ACCOUNTING_START_NAME: RawCollections.ACCOUNTING_START,
            DatabaseStrings.RAW_COLLECTION_ACCOUNTING_STOP_NAME: RawCollections.ACCOUNTING_STOP,
            DatabaseStrings.RAW_COLLECTION_AUTHENTICATION_NAME: RawCollections.AUTHENTICATION,
            DatabaseStrings.RAW_COLLECTION_REQUEST_POLICY_NAME: RawCollections.REQUEST_POLICY,
            DatabaseStrings.RAW_COLLECTION_SGACL_NAME: RawCollections.SGACL_CONTENT
        }

        self._raw_coll_enum_string_convert = {v: k for k, v in self._raw_coll_enum_convert.items()}

    def db_parse_walk(self):
        """
        The main method for the class. Performs a parsing and updating walk of the raw database.
        Walks on each raw log type collection and gets the arrived logs, parses them and updates parsed database.
        :return: None
        """
        # sort all types of logs into a single list by send time
        all_raw_sorted = list()
        for coll in self._raw_collection_names_list:
            curr_list = self._get_raw_logs(coll)
            if not curr_list:
                continue
            all_raw_sorted.extend(curr_list)
            all_raw_sorted.sort(key=lambda raw_log: raw_log[0].get("time"))

        self._parse_raw_logs(all_raw_sorted)
        self._sync_keeper()

    def _get_raw_logs(self, collection_name):
        """
        returns a list of tuples of the raw log message and a collection enum from a collection.
        :param collection_name: the requested collection.
        :return: a list of tuples in the form [(raw_log, collection_enum), ... ] or None if there are none.
        """
        try:
            collection = self._raw_db[collection_name]
            cursor = collection.find({}, {"_id": 1, "msg": 1, "time": 1}).sort(
                "time", ASCENDING).limit(self._N_PULL_RECORDS_LIMIT)
            nfound = cursor.count(True)
            if nfound == 0:
                return None
            res = list(cursor)
            id_lst = list()
            for entry in res:
                id_lst.append(entry["_id"])
            # add enum to the list to remember what collection they came from
            res = [(x, self._raw_coll_enum_convert[collection_name]) for x in res]
            if not DEBUG_DONT_DELETE_RAW:
                # remove raw logs that we've just read from the database
                num_removed = (self._raw_db[collection_name].delete_many({"_id": {'$in': id_lst}})).deleted_count
                if num_removed != nfound:
                    logger2.error("# of records removed not equal to # of records found in %s", collection)

            logger2.info("got %d raw logs from %s", nfound, collection.name)
            return res

        except mongoerrors.PyMongoError as e:
            logger2.error("Unable to execute: %s", e)
        return None

    def _parse_raw_logs(self, logs_list):
        """
        iterate over the sorted raw logs list and parse the message
        :param logs_list: list of tuples of sorted raw syslogs.
        :return: number of items in the input list parsed.
        """
        if logs_list is None:
            return 0

        for tup in logs_list:
            curr_collection = tup[1]
            log = tup[0]
            dic = dict()
            try:
                time = log.get("time")
                # parse the message part of the log to a key-value dictionary format!
                dic = SyslogParser(log['msg']).parse()
                # add the raw id for the log keeper
                dic["raw_id"] = str(log["_id"])
            except Exception as e:
                logger2.error("Failed to parse log. Dropping. error: %s", e)
                continue
            if not dic:
                return 0

            # call a per-collection updating method to update the parsed database.
            self._parse_caller[self._raw_coll_enum_string_convert[curr_collection]](dic, time)
        return len(logs_list)

    def _sync_keeper(self):
        """
        Try to insert deferred logs back to the database.
        :return: None
        """
        # item in list is (log, enum)
        logs_list = self._keeper.get_need_to_sync()
        for (log, enum) in logs_list:
            timestamp = log["time_stamp"]
            # strip timestamp added by keeper for consistency.
            del log["time_stamp"]
            self._parse_caller[self._raw_coll_enum_string_convert[enum]](log, timestamp)

    def _handle_accounting_start_event(self, log_dict, time):
        """
        Handles an accounting start log event and updates the parsed database accordingly.
        :param log_dict: a parsed log (dictionary) of an accounting start event.
        :param time: the time stamp of the arrived log.
        :return: None
        """
        try:
            collection = self._ise_db[DatabaseStrings.ISE_COLL_SESSIONS]
            res = collection.find_one(
                {
                    "CPMSessionID": log_dict.get("CPMSessionID")
                }
            )
            if res:
                # found a record. Update IP addresses.
                res = collection.update_one(
                    {"CPMSessionID": log_dict["CPMSessionID"]},
                    {"$set": {"Framed-IP-Address": log_dict.get("Framed-IP-Address"),
                              "NAS-IP-Address": log_dict.get("NAS-IP-Address")}}
                )
                logger2.info("Existing session updated with accounting start record. User: %s, MAC: %s, Access:%s",
                             log_dict.get("User-Name"), log_dict.get("Calling-Station-ID"),
                             log_dict.get("NAS-IP-Address"))
            else:
                # no record found. Insert it
                collection.insert_one(
                    {"Time": time, "User-Name": log_dict.get("User-Name"),
                     "Framed-IP-Address": log_dict.get("Framed-IP-Address"),
                     "NAS-IP-Address": log_dict.get("NAS-IP-Address"),
                     "CPMSessionID": log_dict.get("CPMSessionID"),
                     "Calling-Station-ID": log_dict.get("Calling-Station-ID"),
                     "SGT": {}}
                )
                logger2.info("New session created with accounting start record. User: %s, MAC: %s, Access:%s",
                             log_dict.get("User-Name"), log_dict.get("Calling-Station-ID"),
                             log_dict.get("NAS-IP-Address"))

        except mongoerrors.PyMongoError as e:
            logger2.error("Unable to execute: %s", e)
        return

    def _handle_accounting_stop_event(self, log_dict, time):
        """
        Handles an accounting stop log event and updates the parsed database accordingly.
        :param log_dict: a parsed log (dictionary) of an accounting stop event.
        :param time: the time stamp of the arrived log.
        :return: None
        """
        try:
            collection = self._ise_db[DatabaseStrings.ISE_COLL_SESSIONS]

            # we need to keep the removed data in memory to find corresponding SGT and remove/dec it.
            deleted_session = collection.find_one_and_delete(
                {"CPMSessionID": log_dict.get("CPMSessionID"),
                 "Calling-Station-ID": log_dict.get("Calling-Station-ID")})

            if deleted_session:
                logger2.info("A session was ended. User: %s, MAC: %s, Access:%s",
                             log_dict.get("User-Name"), log_dict.get("Calling-Station-ID"),
                             log_dict.get("NAS-IP-Address"))

                self._decrease_sgt_count_for_access(deleted_session)
            else:
                # edge case: need to keep this record since it may have arrived before the session was registered.
                self._keeper.keep(log_dict, time, RawCollections.ACCOUNTING_STOP)
                logger2.warning("Got accounting stop but deleted nothing!: %s, MAC: %s, Access:%s",
                                log_dict.get("User-Name"), log_dict.get("Calling-Station-ID"),
                                log_dict.get("NAS-IP-Address"))

        except mongoerrors.PyMongoError as e:
            logger2.error("Unable to execute: %s", e)
        return

    def _handle_authentication_event(self, log_dict, time):
        """
        Handles an authentication log event and updates the parsed database accordingly.
        :param log_dict: a parsed log (dictionary) of an authentication event.
        :return: None
        """

        # verify we've got SGT. We assume only one SGT at this point
        try:
            sgt = log_dict.get("Response", {}).get("cisco-av-pair", {})
            # Handle some weird logs with abnormal structure that hold no data.
            if sgt and type(sgt) is dict:
                sgt = sgt.get("cts:security-group-tag")
                sgt, sgt_ver = sgt.split('-')
            else:
                return
        except KeyError as e:
            sgt = sgt_ver = None
            logger2.warning("Authentication without SGT detected: %s", e)
        # look for existing record. If it does exist, it may contain new layer 3 info (IP) but not SGT.
        # This is why we can't do it in a single update query.
        try:
            collection = self._ise_db[DatabaseStrings.ISE_COLL_SESSIONS]
            res = collection.find_one({"CPMSessionID": log_dict.get("CPMSessionID")})
            if res:
                # found a record. Update SGT.
                res = collection.update_one(
                    {"CPMSessionID": log_dict["CPMSessionID"]},
                    {"$set": {"SGT": {"sgt_id": sgt, "sgt_version": sgt_ver}}})

                logger2.info("Existing session updated with authentication record. User: %s, MAC: %s, Access:%s",
                             log_dict.get("User-Name"), log_dict.get("Calling-Station-ID"),
                             log_dict.get("NAS-IP-Address"))
            else:
                # no record found. Insert it
                collection.insert_one(
                    {"Time": time, "User-Name": log_dict.get("User-Name"),
                     "Framed-IP-Address": log_dict.get("Framed-IP-Address"),
                     "NAS-IP-Address": log_dict.get("NAS-IP-Address"),
                     "CPMSessionID": log_dict.get("CPMSessionID"),
                     "Calling-Station-ID": log_dict.get("Calling-Station-ID"),
                     "SGT": {"sgt_id": sgt, "sgt_version": sgt_ver}}
                )
                logger2.info("New session created with authentication record. User: %s, MAC: %s, Access:%s",
                             log_dict.get("User-Name"), log_dict.get("Calling-Station-ID"),
                             log_dict.get("NAS-IP-Address"))

            # update access devices anyway
            collection = self._ise_db[DatabaseStrings.ISE_COLL_ACCESS]

            res = collection.find_one(
                {
                    "NAS-IP-Address": log_dict.get("NAS-IP-Address")
                })
            if res:
                res2 = collection.update_one(
                    {"NAS-IP-Address": log_dict["NAS-IP-Address"], "SGTs.sgt_id": sgt},
                    {"$inc": {"SGTs.$.counter": 1}})
                if res2.matched_count == 0:
                    # access device exists, but no SGT
                    collection.update_one(
                        {"NAS-IP-Address": log_dict["NAS-IP-Address"]},
                        {"$push": {"SGTs": {"sgt_id": sgt, "counter": 1}}}
                    )
                    logger2.info("New SGT to access device added. IP:%s SGT: %s", log_dict["NAS-IP-Address"], sgt)
                else:
                    logger2.info("Access device IP:%s increases SGT's:%s count by 1", log_dict["NAS-IP-Address"], sgt)
            else:
                # no record at all
                collection.insert_one(
                    {"Time": time, "NAS-IP-Address": log_dict["NAS-IP-Address"],
                     "SGTs": [{"sgt_id": sgt, "counter": 1}]}
                )
                logger2.info("New Access device IP:%s was added with sgt:%s", log_dict["NAS-IP-Address"], sgt)

        except mongoerrors.PyMongoError as e:
            logger2.error("Unable to execute: %s", e)
        return

    def _handle_sgt_request_event(self, log_dict, time):
        """
        Handles a SGT request log event and updates the parsed database accordingly.
        :param log_dict: a parsed log (dictionary) of a SGT request event.
        :param time: the time stamp of the arrived log.
        :return: None
        """

        # response can be of two types:
        # 1. one av-pair in response will be parsed as a dictionary.
        # 2. > 1 av-pairs in response will be parsed to a list of dictionaries.

        dest = log_dict.get("cisco-av-pair", {}).get("cts-rbacl-source-list")
        if not dest:
            logger2.warning("Got a SGT request without destination SGT. Enforcing device:%s", log_dict.get(
                "NAS-IP-Address"))

        # dest may include a version
        if '-' in dest:
            dest = dest.split('-')[0]

        # verify we've got response data
        response = log_dict.get("Response", {}).get("cisco-av-pair", {}).get("cts:src-dst-rbacl")
        if not response:
            logger2.error("Got a SGT request without response. Enforcing device:%s", log_dict.get("NAS-IP-Address"))
            return

        # type 1
        if isinstance(response, str):
            self._handle_single_sgt_response_event(log_dict, time, dest, response)
        # type 2
        elif isinstance(response, list):
            for rsp in response:
                self._handle_single_sgt_response_event(log_dict, time, dest, rsp)
        else:
            logger2.error("Got a SGT request with unpredicted response structure. Enforcing device:%s", log_dict.get(
                "NAS-IP-Address"))
        return

    def _handle_single_sgt_response_event(self, log_dict, time, dest, response):
        """
        Helper method to SGT request in case there are several SGTs in the response. Don't use log_dict to extract
        response again.
        :param log_dict: a parsed log (dictionary) of a SGT request event. For searching only.
        :param time: the time stamp of the arrived log.
        :param response: value of cts:src-dst-rbacl: ... (string)
        :return: None
        """
        if not response:
            return
        try:
            sgacl = response.split('-')[-2]
            source_sgt = response.split('-')[0]
            dest_from_response = response.split('-')[3]
        except (IndexError, ValueError):
            logger2.error("SGT request handler got unknown format cisco-av-pair=cts:src-dst-rbacl in Response: %s.",
                          response)
            return

        # in case we got no destination in the SGT request... should not happen.
        if dest:
            # internal test
            if dest != dest_from_response:
                logger2.warning("SGT request logic warning: dst in request not equal to the one in the response. "
                                "Enforcing device:%s", log_dict.get("NAS-IP-Address"))
            else:
                dest = dest_from_response

        # process the data
        try:
            collection = self._ise_db[DatabaseStrings.ISE_COLL_ENFORCING]
            res = collection.find_one({"NAS-IP-Address": log_dict.get("NAS-IP-Address")})
            if res:
                res2 = collection.update_one(
                    {"NAS-IP-Address": log_dict["NAS-IP-Address"], "Matrix.source_sgt": source_sgt,
                     "Matrix.destination_sgt": dest},
                    {"$set": {"Matrix.$.sgacl": sgacl}})
                if res2.matched_count == 0:
                    # enforcing device exists, but no entry for matrix[src, dst]. Create new
                    collection.update_one(
                        {"NAS-IP-Address": log_dict["NAS-IP-Address"]},
                        {"$push": {"Matrix": {"source_sgt": source_sgt, "destination_sgt": dest, "sgacl": sgacl,
                                              "sgacl_content": {}}}})
                    logger2.info("New Matrix entry was added to enforcing device. IP:%s entry:[%s][%s]->[%s]",
                                 log_dict["NAS-IP-Address"], source_sgt, dest, sgacl)
                else:
                    logger2.info(
                        "Enforcing device's %s matrix entry for src_sgt: %s was updated with dest: %s and sgacl:"
                        " %s",
                        log_dict["NAS-IP-Address"], source_sgt, dest, sgacl)
            else:
                # no record at all
                collection.insert_one(
                    {"Last-Update-Time": time, "NAS-IP-Address": log_dict["NAS-IP-Address"],
                     "Matrix": [
                         {"source_sgt": source_sgt, "destination_sgt": dest, "sgacl": sgacl, "sgacl_content": {}}]}
                )
                logger2.info("New enforcing device IP:%s was added with matrix entry:[%s][%s]->[%s]",
                             log_dict["NAS-IP-Address"], source_sgt, dest, sgacl)

        except mongoerrors.PyMongoError as e:
            logger2.error("Unable to execute: %s", e)

        return

    def _handle_sgacl_request_event(self, log_dict, time):
        """
        Handles a SGACL request log event and updates the parsed database accordingly.
        :param log_dict: a parsed log (dictionary) of a SGACL request event.
        :param time: the time stamp of the arrived log.
        :return: None
        """

        requested_sgacl = log_dict.get("cisco-av-pair", {}).get("cts-rbacl")
        response_sgacl = log_dict.get("Response", {}).get("cisco-av-pair", {}).get("cts:rbacl")
        response_content = log_dict.get("Response", {}).get("cisco-av-pair", {})

        if (not response_sgacl) or (not response_content):
            logger2.error("Got a SGACL content request without data. Enforcing device:%s",
                          log_dict.get("NAS-IP-Address"))
            return

        # #### internal test
        try:
            response_sgacl = response_sgacl.split('-')[0]
        except:
            pass

        if response_sgacl != requested_sgacl:
            logger2.warning(
                "SGT content request logic warning: requested SGACL not equal to the one in the response. Enforcing "
                "device:%s",
                log_dict.get("NAS-IP-Address"))

        # ####

        # filter ACLs in the response
        sgacl_content = dict()
        for key, val in response_content.items():
            if "ace#" in key:
                sgacl_content[key] = val

        try:
            collection = self._ise_db[DatabaseStrings.ISE_COLL_ENFORCING]
            res = collection.find_one({"NAS-IP-Address": log_dict.get("NAS-IP-Address")})
            if res:
                collection.update_one(
                    {"NAS-IP-Address": log_dict["NAS-IP-Address"], "Matrix.sgacl": requested_sgacl},
                    {"$set": {"Matrix.$.sgacl_content": sgacl_content}})
                logger2.info("Enforcing device %s got SGACL: %s content updated.",
                             log_dict["NAS-IP-Address"], requested_sgacl)

            else:
                logger2.error("Got SGACL content request for non-existing enforcing device")
                self._keeper.keep(log_dict, time, RawCollections.SGACL_CONTENT)
                return

        except mongoerrors.PyMongoError as e:
            logger2.error("Unable to execute: %s", e)
        return

    # *** extra helper functions *** #
    def _decrease_sgt_count_for_access(self, removed_session):
        """
        Decreases or removes an SGT count for an access device.
        :param removed_session: a log dictionary of a deleted session.
        :return: None
        """
        if not removed_session.get("SGT"):
            logger2.warning(
                "Got accounting stop for session without SGT: Access: %s",
                removed_session.get("NAS-IP-Address"))
            return
        try:
            access = self._ise_db[DatabaseStrings.ISE_COLL_ACCESS]

            res2 = access.update_one(
                {"NAS-IP-Address": removed_session["NAS-IP-Address"],
                 "SGTs.sgt_id": removed_session["SGT"]["sgt_id"]},
                {"$inc": {"SGTs.$.counter": -1}})

            if res2.matched_count == 0:
                logger2.warning(
                    "Got accounting stop but no session with sgt for access:%s and SGT:%s",
                    removed_session["NAS-IP-Address"],
                    removed_session["SGT"]["sgt_id"])
                return
            else:
                logger2.info("SGT counter decreased for access:%s and SGT:%s",
                             removed_session["NAS-IP-Address"],
                             removed_session["SGT"]["sgt_id"])

                # Remove the embedded SGT documents completely if counter reached zero.
                res2 = access.update_one(
                    {"NAS-IP-Address": removed_session["NAS-IP-Address"],
                     "SGTs.sgt_id": removed_session["SGT"]["sgt_id"]},
                    {"$pull": {"SGTs": {"counter": 0}}}
                )
                if res2.modified_count > 0:
                    logger2.info("SGT counter reached zero and was removed: Access %s and SGT:%s",
                                 removed_session["NAS-IP-Address"],
                                 removed_session["SGT"]["sgt_id"])

        except mongoerrors.PyMongoError as e:
            logger2.error("Unable to execute: %s", e)
        return

    def close(self):
        """
        Close the connection to the database.
        :return: None
        """
        self._client.close()
        self._client = None
        logger2.info('Connection closed')

    # private members
    _raw_coll_enum_string_convert = dict()
    _raw_coll_enum_convert = dict()
    _parse_caller = dict()
    _client = None
    _raw_db = None
    _ise_db = None
    _keeper = None
