#!/usr/bin/env python

from src.db_updater import DbUpdater, DatabaseStrings
from src.ise_parser import SyslogParser


def fake_parse_raw_logs(dbu, logs_list):
    """
    USED FOR TESTING ONLY
    iterate over the sorted raw logs list and parse the message
    :param dbu: database instance
    :param logs_list: list of tuples of sorted raw syslogs.
    :return: number of items in the input list parsed.
    """
    if logs_list is None:
        return 0

    for tup in logs_list:
        curr_collection = tup[1]
        log = tup[0]
        dic = dict()
        try:
            time = log.get("time")
            # parse the message part of the log to a key-value dictionary format!
            dic = SyslogParser(log['msg']).parse()
            # add the raw id for the log keeper
            dic["raw_id"] = str(log["_id"])
        except Exception as e:
            print("Failed to parse log. Dropping")
            continue
        if not dic:
            return 0

        # call a per-collection updating method to update the parsed database.
        dbu._parse_caller[dbu._raw_coll_enum_string_convert[curr_collection]](dic, time)
    return len(logs_list)


def test_accounting_start():
    dbu = DbUpdater()
    raw_logs = dbu._get_raw_logs(DatabaseStrings.RAW_COLLECTION_ACCOUNTING_START_NAME)
    fake_parse_raw_logs(dbu, raw_logs)
    dbu.close()


def test_accounting_stop():
    dbu = DbUpdater()
    raw_logs = dbu._get_raw_logs(DatabaseStrings.RAW_COLLECTION_ACCOUNTING_STOP_NAME)
    fake_parse_raw_logs(dbu, raw_logs)
    dbu.close()


def test_authentication():
    dbu = DbUpdater()
    raw_logs = dbu._get_raw_logs(DatabaseStrings.RAW_COLLECTION_AUTHENTICATION_NAME)
    fake_parse_raw_logs(dbu, raw_logs)
    dbu.close()


def test_sgt_request():
    dbu = DbUpdater()
    raw_logs = dbu._get_raw_logs(DatabaseStrings.RAW_COLLECTION_REQUEST_POLICY_NAME)
    fake_parse_raw_logs(dbu, raw_logs)
    dbu.close()


def test_sgacl_content():
    dbu = DbUpdater()
    raw_logs = dbu._get_raw_logs(DatabaseStrings.RAW_COLLECTION_SGACL_NAME)
    dbu._parse_raw_logs(DatabaseStrings.RAW_COLLECTION_SGACL_NAME, raw_logs)
    dbu.close()


def test_parse_all():
    dbu = DbUpdater()
    dbu.db_parse_walk()
    dbu.close()


def main():
    # test_accounting_start()
    # test_authentication()
    # test_accounting_stop()
    # test_sgt_request()
    # test_sgacl_content()
    test_parse_all()


if __name__ == '__main__':
    main()
