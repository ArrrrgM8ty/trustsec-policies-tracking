# Cisco TrustSec Security Policies Tracking - Technion Project #

This is the repository for the *Security Policies Tracking in Organization Network Project* given by Cisco for the Technion [LCCN Lab](http://lccn.cs.technion.ac.il) during Spring 2017.

### Overview and Goal ###

Information on what is Cisco's TrustSec Network can be found [here](https://www.cisco.com/c/en/us/td/docs/switches/lan/trustsec/configuration/guide/trustsec/arch_over.html).

By parsing Cisco Identities Service Engine (Cisco ISE) event logs, the system constructs an updated and near real-time representation of a network's security policies being enforced in the TrustSec enabled network. This information is presented to the user via web interface, enabling execution of predefined queries.

The goal of the system is to allow the network administrator a more low level inspection of the deployed security policies in the network.

The system includes an emulator to allow simulation of Syslogs sent by ISE.

### Prerequisites ###

* Python 3.5
* PyVenv - the python virtual environment
* MongoDB (local instance)
* HTTP server (tested with nginx)
* Linux host machine (tested with Ubuntu 16.04 LTS) 

### Deploying on a Host Machine ###

This repository includes an `install.sh` file, though it is probably best to use this file as a guideline only.

* Install all of the requirements listed in the installation file.
* Install nginx web server.
* Follow the instructions [here](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uwsgi-and-nginx-on-ubuntu-16-04)
 on how to deploy the server and configure it as a service.
* Make frontend and backend into a service with [this](http://patrakov.blogspot.co.il/2011/01/writing-systemd-service-files.html).
* Reboot the system.
* Make sure both backend and frontend work with the command `systemctl status ise-back.service ise-front.service`
* To test that everything works, run `python3 /src/ise_emulator.py` on the host machine and follow the on screen IPs. You should be able to query for these IPs and seen them in the web interface.
* (Optional) Download and install [robomongo](https://robomongo.org/) - a MongoDB management GUI. You may find this useful for debugging the database. You can tunnel MongoDB's default port (27017) via SSH to be able to connect to your remote host's local MongoDB instance.  

### Uninstalling ###

Simply undo everything in `install.sh` manually.