def reorder_matrix(matrix):
    results = []
    user_sgt = []
    resource_sgt = []

    for cell in matrix:
        user_sgt.append(cell['source_sgt'])
        resource_sgt.append(cell['destination_sgt'])

    user_sgt = sorted(list(set(user_sgt)))
    resource_sgt = sorted(list(set(resource_sgt)))

    for i, r in enumerate(resource_sgt):
        results.append([])
        for u in user_sgt:
            flag = False
            for cell in matrix:
                if cell['source_sgt'] == u and cell['destination_sgt'] == r:
                    content = [val.replace(';','') for val in cell['sgacl_content'].values()] # for striping remaining ;
                    results[i].append({'sgacl': cell['sgacl'], 'content': content})
                    flag = True
                    break
            if not flag:
                results[i].append(None)
    return user_sgt, resource_sgt, results