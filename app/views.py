from flask import Flask, render_template
from flask_pymongo import PyMongo

from .forms import IPForm, SGTForm, CellForm
from app.aux import reorder_matrix as rm
from src.queries import Queries

app = Flask(__name__)
app.config.from_object('config')
mongo = PyMongo(app)

@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    return render_template('index.html',
                           title='Home')


@app.route('/about', methods=['GET'])
def about():
    return render_template('about.html',
                           title='About')


@app.route('/contact', methods=['GET'])
def contact():
    return render_template('contact.html',
                           title='Contact Us')


@app.route('/query/ip', methods=['GET', 'POST'])
def query_ip():
    form = IPForm()
    if form.validate_on_submit():
        params = {}
        params['ip'] = form.ip.data
        col = mongo.db.access
        enf_results = Queries.find_enforcing_device_by_ip(mongo.db, params['ip'])
        acc_results = Queries.find_access_device_by_ip(mongo.db, params['ip'])
        if enf_results is not None:
          users, resources, enf_results = rm(enf_results['Matrix'])
          params['users'] = users
          params['resources'] = resources
        if acc_results is not None:
          acc_results = acc_results['SGTs']
        return render_template('results.html',
                               title='Results',
                               params=params,
                               enf_results=enf_results,
                               acc_results=acc_results)
    else:
        return render_template('query_ip.html',
                               title='Query IP',
                               form=form)


@app.route('/query/sgt', methods=['GET', 'POST'])
def query_sgt():
    form = SGTForm()
    if form.validate_on_submit():
        params = {}
        params['sgt'] = form.sgt.data
        params['type'] = 'Access' if form.stype.data == 'acc' else 'Enforcer'
        sgt_results = Queries.find_devices_by_sgt(mongo.db, params['sgt'], params['type'])
        print(sgt_results)
        if sgt_results is not None:
          sgt_results = [ device['NAS-IP-Address'] for device in sgt_results ]
        return render_template('results.html',
                               title='Results',
                               params=params,
                               sgt_results=sgt_results)
    else:
        return render_template('query_sgt.html',
                               title='Query SGT',
                               form=form)


@app.route('/query/cell', methods=['GET', 'POST'])
def query_cell():
    form = CellForm()
    if form.validate_on_submit():
        params = {}
        params['usgt'] = str(form.usgt.data)
        params['rsgt'] = str(form.rsgt.data)
        params['ip'] = form.ip.data
        cell_results = Queries.find_enforcing_device_by_ip(mongo.db, params['ip'])
        if cell_results is not None:
          flag = False
          for cell in cell_results['Matrix']:
            if cell['destination_sgt'] == params['rsgt'] and cell['source_sgt'] == params['usgt']:
              cell_results = { k: v.replace(';','') for k,v in cell['sgacl_content'].items()}
              flag = True
          if not flag:
            cell_results = None
        return render_template('results.html',
                               title='Results',
                               params=params,
                               cell_results=cell_results)
    else:
        return render_template('query_cell.html',
                               title='Query Cell',
                               form=form)


if __name__ == "__main__":
  app.run(host='0.0.0.0')
