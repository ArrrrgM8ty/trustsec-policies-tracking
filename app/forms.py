from flask_wtf import Form
from wtforms import IntegerField, RadioField, StringField
from wtforms.validators import InputRequired, IPAddress, NumberRange



class IPForm(Form):
    ip = StringField('ip', validators=[InputRequired(), IPAddress()], render_kw={"placeholder": "0.0.0.0"})



class SGTForm(Form):
    sgt = IntegerField('sgt', validators=[InputRequired(), NumberRange(min=0, message="Invalid SGT")], render_kw={"placeholder": "0"})
    stype = RadioField('stype', choices=[('acc', 'Access'), ('enf', 'Enforcer')], validators=[InputRequired()])



class CellForm(Form):
    usgt = IntegerField('usgt', validators=[InputRequired(), NumberRange(min=0, message="Invalid SGT")], render_kw={"placeholder": "0"})
    rsgt = IntegerField('rsgt', validators=[InputRequired(), NumberRange(min=0, message="Invalid SGT")], render_kw={"placeholder": "0"})
    ip = StringField('ip', validators=[InputRequired(), IPAddress()], render_kw={"placeholder": "0.0.0.0"})